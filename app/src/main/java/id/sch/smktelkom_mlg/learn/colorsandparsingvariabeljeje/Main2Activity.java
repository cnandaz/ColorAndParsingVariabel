package id.sch.smktelkom_mlg.learn.colorsandparsingvariabeljeje;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView txtNama, txtTahun, txtAlamat, txtTelepon, txtEmail;
    Button btnnext, btnexit;
    String get_nama, get_alamat, get_email, get_telepon;
    Integer get_tahun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        txtNama = findViewById(R.id.txtNama);
        txtTahun = findViewById(R.id.txtTahun);
        txtAlamat = findViewById(R.id.txtAlamat);
        txtTelepon = findViewById(R.id.txtTelepon);
        txtEmail = findViewById(R.id.txtEmail);
        btnnext = findViewById(R.id.btnnext);
        btnexit = findViewById(R.id.btnexit);

        Bundle b = getIntent().getExtras();
        get_nama = b.getString("parse_nama");
        get_tahun = b.getInt("parse_tahun");
        get_alamat = b.getString("parse_alamat");
        get_telepon = b.getString("parse_telepon");
        get_email = b.getString("parse_email");

        txtNama.setText("Nama Anda adalah " + get_nama);
        txtTahun.setText("Tahun lahir Anda ");
        txtAlamat.setText("Alamat Anda adalah " + get_alamat);
        txtTelepon.setText("Nomer Telepon Anda " + get_telepon);
        txtEmail.setText("Email Anda adalah " + get_email);

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = null;
                i = new Intent(Main2Activity.this, RadioColor.class);
                startActivity(i);
            }
        });

        btnexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}